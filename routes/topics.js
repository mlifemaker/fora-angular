var express = require('express');
var router = express.Router();
var model = require('../model/model')
var datasource = require('../service/datasource')

/* GET topics listing. */
router.get('/:id', function(req, res) {

    var topic = datasource.topics.find(function(t){
        return t.get("id")==req.params.id
    })

    res.json(topic)
});

module.exports = router;