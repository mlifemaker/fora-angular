var express = require('express');
var router = express.Router();
var model = require('../model/model')
var datasource = require('../service/datasource')

/* GET users listing. */
router.get('/', function(req, res) {



  res.json(datasource.users);
});

router.get('/all', function(req, res) {
    res.json(datasource.users.sortBy("name"))
});


router.get('/admins', function(req, res) {


    res.json(datasource.users.filter(function(user){
        return user.get("admin")
    }))
});

router.put('/:id', function(req, res) {

    var user = req.body;
    console.log("&&&&&&")
    console.log("req.params.id")
    console.log(req.body)
    var userInDatasource = datasource.users.find(function(user){
            return user.id == req.params.id;
    })

    userInDatasource.set("statement", user.statement);
    userInDatasource.set("admin", user.admin);
    console.log(userInDatasource.toJSON())

    res.json(datasource.users.sortBy("name"));
});




module.exports = router;
