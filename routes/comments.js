var express = require('express');
var router = express.Router();
var model = require('../model/model')
var datasource = require('../service/datasource')

/* GET topics listing. */
router.get('/:id', function(req, res) {

    console.log("here")
    var comment = datasource.comments.find(function(t){
        return t.get("id")==req.params.id
    })
    //console.log(topic)

    res.json(comment)
});

router.put('/:id', function (req, res){
    console.dir(req.body)

    var comment = datasource.comments.find(function(t){
        return t.get("id")==req.params.id
    });
    comment.attributes = req.body;

    res.json({ok:true})

});

module.exports = router;