/**
 * Created by  User: nicorama
 * Date: 20/10/2014 ; Time: 13:05
 */


var app = angular.module("fora");

app.controller("UsersController", function($scope, dataService, $http){

    $scope.data = []


    /****** Delete this at the start of the demo */
    $scope.fetchUsers = function(){
        $http.get('users/all').success(function (data) {
            console.log("fetched this", data)
            $scope.data = data;
        });
    }

    $scope.setAdmin = function(user){
        user.statement =" Woooohooo I'm an admin"
        user.admin = true;

        $http.put('users/'+user.id, user).success(function (data) {
            console.log("updated this", data)
            $scope.data = data;
        });
    }

})