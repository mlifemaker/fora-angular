/**
 * Created by  User: nicorama
 * Date: 20/10/2014 ; Time: 14:07
 */
angular.module("fora")

    .factory("dataService", function ($http) {

        return {

            fetchUsers: function ($scope) {
                console.log("in fetchUsers")
                $http.get('users/all').success(function (data) {
                    console.log("fetched this", data)
                    $scope.data = data;
                });
            }
        }

    })