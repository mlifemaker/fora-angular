var model = require('../model/model')

var nicolas = {admin: true, id: 1, email: "nz@robusta.io", name: "Nicolas", statement: "Star Wars rocks"},
    leonard = {admin: true, id: 2, email: "leonard@robusta.io", name: "Leonard", statement: "Star Trek rocks"},
    sheldon = {id: 3, email: "sheldon@robusta.io", name: "Sheldon"},
    raj = {id: 4, email: "raj@robusta.io", name: "Rajesh"},
    howard = {id: 5, email: "howie@robusta.io", name: "Howard"},
    penny = {admin: true, id: 6, email: "penny@robusta.io", name: "Penny", statement: "Penny ! Penny ! Penny !"},
    emy = {id: 7, email: "emy@robusta.io", name: "Emy"},
    bernie = {id: 8, email: "bernie@robusta.io", name: "Bernadette"}


var users = new model.UserCollection([nicolas, leonard, sheldon, raj, howard, penny, emy, bernie])

var flag = {id: 1, content: "This guy went too far."}


var c1 = {id: 1, anonymous: true, content: "I'm not ok"},
    c2 = {id: 2, user: leonard, content: "You don't know enough about heroes"},
//we'll need to describe flags as a collection
    c3 = {id: 3, anonymous: true, content: "What ? You stupid !", flags: [flag]}

var comments = new model.CommentCollection([c1, c2, c3]);


var troll = {id: 1, title: "Star Trek > Star Wars", content: "*Spock* is stronger than Yoda",
    user: leonard, comments: [c1, c2, c3]}

var kids = {id: 2, title: "Kids ar' cool",
    content: "But I'm so <strong>tired</strong> <script type='text/javascript'>alert('you are fired!')</script>",
    user: leonard, comments: []}

var topics = new model.TopicCollection([troll, kids])


exports.users = users;
exports.topics = topics;
exports.comments = comments;