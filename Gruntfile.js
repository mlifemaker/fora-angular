// Generated on 2014-06-11 using generator-angular 0.9.0-1
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {



  // Configurable paths for the application
  var appConfig = {
    dist: 'dist'
  };

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Test settings
    karma: {
      unit: {
        configFile: 'karma/karma.conf.js',
        singleRun: true
      }
    }
  });




  grunt.registerTask('test', [
    'karma'
  ]);


};
