//That can't work with AMD modules ; it could work with browserify
var Backbone = require("backbone")


var Topic = Backbone.Model.extend({

    url : function(){
        return "topic/"+this.get("id")
    },
    toString : function(){
        return this.get("title");
    }
});


var Comment = Backbone.Model.extend({

    url : function(){
        return "comment/"+this.get("id")
    },
    toString : function(){
        return this.get("content");
    }
});




var User = Backbone.Model.extend({

    url : function(){
        return "user/"+this.get("id")
    },
    toString : function(){
        return this.get("name");
    },

    isAdmin : function(){
        //TODO
    }
});


var TopicCollection = Backbone.Collection.extend({
    model : User,


    url : "users"
});

var CommentCollection = Backbone.Collection.extend({
    model : User,
    url : "users"
});

var UserCollection = Backbone.Collection.extend({
    model : User,


    url : "users"
});


var AdminCollection = new UserCollection();
AdminCollection.parse = function(){
    //stuff
}


exports.Comment = Comment;
exports.User = User;
exports.UserCollection= UserCollection;
exports.CommentCollection= CommentCollection;
exports.TopicCollection= TopicCollection;

/*
 var AdminCollection =  Backbone.Collection.extend({
 model : User,

 parse : function(){
 //stuff
 }
 });
 */